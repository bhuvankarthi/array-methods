# Array and it's Methods :


## What is an Array
* An Array is a collection of similar types of data , In JavaScript all the arrays are objects , we can perform various functions with the arrays as mentioned below .


## Methods used in an Array 



 ### Array.join()
   * This method joins every elements in an array to a string .
   
* Example:

   * let arr=[x,y,x];
   * console.log(arr.join(""))

   * output : "xyx"

    
### Array.flat()
   * It returns a new array with all the sub-arrays concatenated for our specific depth .

* Example :
    * console.log([1,2,[3,4],5,[6,7]].flat()); 

    * output : [1,2,3,4,5,6,7]


### Array.indexOf()
* It returns the first occurence of the parameter given inside the indexOf function .

* Exmaple :
    * let arr=[1,2,3,3,4];

    * console.log(arr.indexOf(3));

    * output : 2   //prints the index of 3

### Array.lastIndexOf()
* It returns the last occurence of the parameter given inside the lastIndexOf function .

* Example :
    * let arr=[1,2,3,3,4,4];

    * console.log(arr.lastIndexOf(4));

    * output : 5 // prints the last occurence of 4 

### Array.includes()
* It returns a boolean value whether the array includes the given element .

* Example :
    * let arr=[1,2,3,4];

    * console.log(arr.includes(3))

    * output : true //prints true because 3 is included in arr

### Array.every()
* It checks for every elements in the array and return true if all elements satisfies the specific condtition else returns false .

* Example :
    * let arr=[1,2,3]

    * console.log(arr.every(el=>el>1)) // prints false As 1 is not greater than 1 .

    * output : false 

### Array.reverse()
* It returns the reversed array of the original array .

* Example:
    * let arr=[1,2,3];

    * console.log(arr.reverse());

    * output : [3,2,1] //reversed array

### Array.shift()
* It removes the very first element of the array and returns that element .

* Example:
    * let arr=[1,2,3];

    * console.log(arr.shift());

    * output : 1 //first element of the array (arr)

### Array.unshift()
* It add an element which is declared inside the unshift function to the first index of the given array .

* Example:
    * let arr=[1,2,3];

    * console.log(arr.unshift(5));

    * output : [5,3,2,1] //final array

### Array.push()
* It adds a new element to the last of the given array .

* Example:
    * let arr=[1,2,3];

    * console.log(arr.push(5));

    * output : [1,2,3,5] //adds 5 to the last 

### Array.pop()
* It pops the last element from the given array and returns it .

* Example:
    * let arr=[1,2,3];

    * console.log(arr.pop());

    * output : 3 //last element 3 is popped and printed 

### Array.splice()
* This method changes the given array according to specific conditions given .

1. Example:
    * let arr=[1,2,3,4];

    * console.log(arr.splice(1,3));  // splice(0,2) => 1 is the starting index and 3 is number of elements from the starting index .

    * output : [2,3] //final array that changes the array with only 3 elements from index 1 .

### Array.find()
* It returns the first value of the given array that satisfies the required condition .

1. Example:
    * let arr=[1,2,3,4];

    * console.log(arr.find(el=>el>2));

    * output : 3 //both 3 and 4 are greater than 2 but it returns the first value that satisfies the condition .

### Array.findIndex(0)
* I returns the first element's index that satisfies the specific condition , If no elements satisfies the condition it returns -1 .

1. Example:
    * let arr=[1,2,3];

    * console.log(arr.findIndexOf(el=>el>1));

    * output : 1 // As 2 and 3 is greater than 1 , it returns the first element's index , so 1 is the index of 2 .

### Array.filter()
* It returns a new array in which the elements from the given array that satisfies the specific condition .

1. Example:
    * let arr=[1,2,3,4];

    * console.log(arr.filter(el=>el>2));

    * output : [3,4] //It filters the array and returns a new array according to our condition .

### Array.map()
* It returns a new array performing a specified operation on every element of the given array .

1. Example:
    * let arr=[1,2,3];

    * console.log(arr.map(el=>el+2));

    * output : [3,4,5] //adds 2 to every element of the array 

### Array.forEach()
* It iterates through each element of the array but does not return an array .

1. Example:
    * let arr=[1,2,3];

    * let array=[]

    * arr.forEach(el=>{
        array.push(el+3)
    });

    *console.log(array)

    * output : [4,5,6] //adds 3 to every element but here we have to push those results to an empty array .

### Array.reduce()
* I returns a value after performing a specific operation within the elements of the given array .

1. Example:
    * let arr=[1,2,3];

    * console.log(arr.reduce((a,c)=>a+c),0);

    * output : 6 // It adds every elements as the initial value is 0 as initialized .

### Array.slice()
* It returns a new array with a specific limit of elements from the given array .

1. Example:
    * let arr=[1,2,3,4,5];

    * console.log(arr.slice(2,4))

    * output : [3,4,5] // It returns the elements between given index in the new array without changing the existing one .

### Array.some()
* It returns true if atleast one element of the array satisfies the given condition .

1. Example:
    * let arr=[1,2,3,4,3];

    * console.log(arr.some(el=>el>3))

    * output : true // element 4 from the array satisfies the given consition , so it returns true .





    